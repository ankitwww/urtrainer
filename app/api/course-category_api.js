﻿
var app = require('express').Router();


var userasProvider = require('../../app/models/user');
var allcourses = require('../../app/models/allcourse');
var trainings_category = require('../../app/models/trainings_category');

app.get('/api/course_category/:course_cat_name', function (req, res) {
    //console.log("|" + req.params.course_cat_name + "|");
    allcourses.find({ Domain: req.params.course_cat_name }, function (err, docs) {
        var tc = docs.length;
        if (err) { throw err; } 
        else {
            var mycnt = 0;
            docs.forEach(function (CUPitem) {
                
                var thisproviderid = CUPitem.providerid;
                
                userasProvider.find({ userid: thisproviderid }, function (err, CUPdocs) {
                    if (err) {
                        throw err;
                    } 
                    else {
                        mycnt++;
                        //CUPitem._doc.push(CUPdocs[0]._doc);
                        CUPitem._doc.email = CUPdocs[0].email;
                        CUPitem._doc.mobile = CUPdocs[0].mobile;
                        CUPitem._doc.role = CUPdocs[0].role;
                        CUPitem._doc.instiName = CUPdocs[0].instiName;
                        CUPitem._doc.Address1 = CUPdocs[0].Address1;
                        CUPitem._doc.Address2 = CUPdocs[0].Address2;
                        CUPitem._doc.City = CUPdocs[0].City;
                        CUPitem._doc.State = CUPdocs[0].State;
                        CUPitem._doc.Country = CUPdocs[0].Country;
                        CUPitem._doc.Contact2 = CUPdocs[0].Contact2;
                        CUPitem._doc.Youare = CUPdocs[0].Youare;
                        CUPitem._doc.Logo = CUPdocs[0].Logo;
                        
                        if (mycnt >= tc) {
                            
                            res.send(JSON.stringify(docs));
                        }
                    }
                });
            });
        }


    });

                    
});


app.get('/api/course_category_items/:course_cat_name', function (req, res) {
    
    trainings_category.find({ training_cat: req.params.course_cat_name }, function (err, docs) {
        if (!err) {

        } else { throw err; }
        res.send(JSON.stringify(docs));

    });
});


module.exports = app;
﻿var app = require('express').Router();
var userasProvider = require('../../app/models/user');
var allcourses = require('../../app/models/allcourse');

app.get('/api/getProviderDetails', function (req, res) {
    res.send(JSON.stringify(req.user._doc));
})

app.get('/api/getCourseList/:providerid', function (req, res) {
    console.log(req.user._doc);
    var providerid = req.params.providerid;
    allcourses.find({ providerid: providerid,StatusByP: {$ne:'deleted'} }, function (err, docs) {
        res.send(JSON.stringify(docs));
    })
})

// Updating provider profile as profile is automatically created with some data
app.post('/api/ProviderPage/profileupdate', function (req, res) {
    var provider = (req.body);
    var profileid = provider.id;
    var mydate = new Date();
    provider.LatupdateDate = mydate.toDateString();
    provider.Status = "Pending";
    if (!provider.instiName || !provider.Address1 || !provider.City || !provider.State || !provider.Country || !provider.Pin || !provider.Contact1) {
        res.send("validationError");
    }
    else {
        userasProvider.update({ userid: profileid }, provider, function (err) {
            if (err) {
                res.send("error");
                console.log(err);
            }
            else {
                res.send("updated");
            }
        })
    }
})

// CRUD Operation on course
app.post('/ProviderPage/:posttype', function (req, res) {
    if (req.params.posttype == 'addcourse') {
        
        // Extracting providerdata from database
        var course = (req.body);
        var allcourse = new allcourses();
        
        
        allcourse.providerid = course.userid;
        allcourse.Domain = course.Domain;
        allcourse.TrainingName = course.TrainingName;
        allcourse.classtype = course.classtype;
        allcourse.PrimaryTag = course.PrimaryTag;
        allcourse.SecondaryTag = course.SecondaryTag;
        allcourse.Startdate = course.Startdate;
        allcourse.Enddate = course.Enddate;
        //allcourse.days = course.days;
        allcourse.Fee = course.Fee;
        allcourse.TopicsToCover = course.TopicsToCover;
        allcourse.Video = course.Video;
        allcourse.TeacherDetail = course.TeacherDetail;
        allcourse.Comments = course.Comments;
        
        var mydate = new Date();
        allcourse.date = mydate.toDateString();
        allcourse.StatusByP = course.StatusByP;
        allcourse.StatusByA = "Pending";
        
        allcourses.count({}, function (err, c) {
            allcourse.courseID = c;
            if (err) {
                res.send("error in creating courseID");
            }
            else {
                allcourse.save(function (err) {
                    if (err) {
                        res.send(err);
                        console.log('error' + err);
                    }
                    else {
                        res.send("updated");
                        console.log('success');
                    }
                });
            }
        });


    }
    
    // Update course
    if (req.params.posttype == 'updatecourse') {
        
        var course = (req.body);
        
        if (!course.Domain || !course.TrainingName || !course.classtype || !course.PrimaryTag || !course.SecondaryTag || !course.Startdate || !course.Enddate || !course.Fee ) {
            res.send("validationError");
        }
        else {
            // var ppp = userasProvider.find({ "userid": id });
            allcourses.update({ _id: course._id }, course, function (err) {
                if (err) {
                    console.log('error' + err);
                    res.send("error");
                }
                else {
                    res.send("updated");
                    console.log('success');
                }
            });
        }
    }
    
    // Remove course
    if (req.params.posttype != 'removecourse') {
    } else {
        console.log('controller calls.');
        var course = (req.body);
        var id = course._id;
        // var ppp = userasProvider.find({ "userid": id });
        allcourses.update({ _id: course._id }, { $set: { StatusByP: 'deleted' } }, function (err) {
            if (err) {
                console.log('error' + err);
            } else {
                res.send("updated");
                console.log('successfully deleted' + id);
            }
        });
    }
});


//==================== edit course will get data of course from here ================editcourse

app.get('/api/ProviderPage/editcourse/:courseid', function (req, res) {
    
    var courseid = req.params.courseid;
    
    allcourses.find({ _id : courseid }, function (err, docs) {
        if (!err) {
        } else { throw err; }
        res.send(JSON.stringify(docs));

    });

});


module.exports = app;
﻿var mongoose = require('mongoose');

var locationSchema = mongoose.Schema({
    State: String,
    City: String
})

module.exports = mongoose.model('locations', locationSchema);
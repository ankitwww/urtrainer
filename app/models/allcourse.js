﻿// load the things we need
var mongoose = require('mongoose');
//var bcrypt = require('bcrypt-nodejs');

// define the schema for our user model
var allcourseSchema = mongoose.Schema({
    courseID:String,
    id:String,
    providerid : String,
       
    
    Domain : { type: String, required: true },
    TrainingName : { type: String, required: true },
    classtype : { type: String, required: true },
    PrimaryTag : { type: String, required: true },
    SecondaryTag : { type: String, required: true },
    Startdate : { type: String, required: true },
    Enddate : { type: String, required: true },
    days : String,
    Fee : { type: String, required: true },
    TopicsToCover : { type: String, required: true },
    Video : String,
    TeacherDetail : String,
    Comments : String,
    
    date: String,
    StatusByP : { type: String, required: true },
    StatusByA : { type: String, required: true },
    Deleted : String
    

});

//allcourseSchema.plugin(autoIncrement.plugin, 'allcourse');

// create the model for users and expose it to our app
module.exports = mongoose.model('allcourse', allcourseSchema);

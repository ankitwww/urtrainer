﻿// load the things we need
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({
    userid : { type: String },
    authSource : { type: String, required: true },
    token : { type: String, required: true },
    email : String,
    password : String,
    seeker_profile : {
        first_name : String,
        last_name : String
    },
    loyalty_points : Number,
    mobile : { type: String, required: false },
    gender : { type: String, required: false },
    role : String,
    counterId: { type: String, required: true },
    InitDate: String,
    
    // Extra data update
    providerid : { type: String, required: false }, // email 
    instiName : { type: String, required: false },
    Address1 : { type: String, required: false },
    Address2 : { type: String, required: false },
    City : { type: String, required: false },
    State : { type: String, required: false },
    Country :  { type: String, required: false },
    Pin :  { type: String, required: false },
    Contact1 : String,
    Contact2 : String,
    Youare : String,
    CV : String,
    Logo: String,
    Images: {
        img1: String,
        img2: String,
        img3: String,
        img4: String,
        img5: String,

    },
    Video: String,
    Registration: { type: String, required: false },
    whatyouoffer:  { type: String, required: false },
    Comments:  { type: String, required: false },
    teacherdetail: String,
    discriptionfile: String,
    //----------------- Admin use data

    
    LatupdateDate : String,
    Status: String,
    Deleted : String

});

// generating a hash
//userSchema.methods.generateHash = function(password) {
//    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
//};

userSchema.pre('save', function (next) {
    var user = this;
    var SALT_FACTOR = 5;
    
    if (!user.isModified('password')) return next();
    
    bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
        if (err) return next(err);
        
        bcrypt.hash(user.password, salt, null, function (err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
});

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('users', userSchema);

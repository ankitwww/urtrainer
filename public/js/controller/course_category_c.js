﻿var app = angular.module("ngcoursecategory", []);

//app.config(function ($routeProvider) {
//    $routeProvider
//    .when('/course_category.test', function () { 
//    Url:'/views/course_category.ejs'
//    });
//});


app.controller("course_category_controller",  ['$scope', 'myService',  '$http', function ($scope, myService, $http) {
        console.log("get request is up");

        //$http.get('/course_category/'+$('#course_name').text()).then(function (response) {
        //    $scope.data = response.data;
        //    //myService.set(response);
        //   // callme();
        //    //$scope.data = response.data;
        //    //$scope.userName = response.data.username;
        //    //$scope.providerallitems = response.data.providerallitems;
        //    //$scope.profile = response.data.providerprofile[0];
        //    //myService.set(response.data.id);
        //    ////console.log("from response" + response.data.providerprofile);
        //    //console.log("from response" + response.data.providerprofile[0].city);
        //    //console.log("from response" + response.data.id);
        //});

       // function callme() {
        $http.get('/api/course_category/' + $('#course_name').text()).then(function (response) {

                 $scope.data = response.data;
            console.log(response.data);
               
                //myService.set(response);
        });
        
        $http.get('/api/course_category_items/' + $('#course_name').text()).then(function (response) {
            $scope.catname = response.data[0].training_cat;
            $scope.catitems = response.data[0].cat_datas.split(",");
            console.log(response.data);
                //myService.set(response);
        });
       

        $http.get('/api/indexlogin').then(function (response) {
            
            console.log(response.data.user);
            
            if (response.data.user == "unknown") {
                $('#logindiv').show();
                $('#logout').hide();
            }
            else {
                // $scope.LUN = response.data.user;
                
                $('#LUN').append('<i class="fa fa-user"></i><i class="fa f-uppercase f-size-user">' + response.data.user + '</i>');
                
                $('#logindiv').hide();
                $('#logout').show();
            }


        });

    }]);


app.factory('myService', function () {
    var savedData = {}
    function set(data) {
        savedData = data;
    }
    function get() {
        return savedData;
    }
    
    return {
        set: set,
        get: get
    }

});

app.controller('navigationcontroller', ['$scope', '$http', function ($scope, $http) {
        
        
        $http.get('/api/indexlogin').then(function (response) {
            
            if (response.data.user == "unknown") {
                $('#logindiv').show();
                $('#logout').hide();
            }
            else {
                $scope.LUN = response.data.user;
                // $('#LUN').append('<i class="fa fa-user"></i><i class="fa f-uppercase f-size-user">' + response.data.user+'</i>');
                
                $('#logindiv').hide();
                $('#logout').show();
            }


        });
    
    }]);